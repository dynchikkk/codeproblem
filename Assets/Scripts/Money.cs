using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    [SerializeField] private int MoneyCount = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>() != null)
        {
            Game.main.Money += MoneyCount;
            Game.main.SetMoneyText();
            GetComponentInChildren<AudioSource>().Play();
            Destroy(gameObject, 0.1f);
        }
    }
}
